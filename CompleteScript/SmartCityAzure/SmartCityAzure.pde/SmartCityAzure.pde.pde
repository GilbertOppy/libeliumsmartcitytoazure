/*
    ------ Connecting Libelium Smart Cities Plug & Sense! through to Azure IoTHub --------
    This script takes Temperature, Pressure, Humidity and O2 level data and sends it through to Azure
    once per minute, timestamping with the devies serial Id also included in the HTTP packet.
   Setup: 
          A. Switch ON WIFI, 
          B. Set ESSID,
          C. Set Trusted CA (WIFI_PRO configured to disable SSL)
          D. Switch OFF WIFI         
          E. Turn on the sensors and RTC
           
   Loop: 
          1. Switch ON WIFI  
          2. Check if connected         
          3. NTP server for Time Sync
          4. Set RTC Time from WiFi module settings
          5. TCP
              5.1. Open TCP socket 
              5.2. SSL hand shake
              5.3. Send Data
              5.4. Wait for answer from server 
              5.5. close socket                                     
          6. Switch OFF WIFI and wait for 45 seconds
             (ensuring 45 + 15 secs for wifi setup means one message sent per min)
             
    TO GET THE DEVICE TO WORK WITH THIS CODE:
    SOCKET B = Temperature,Humidity AND Pressure Probe
    SOCKET C = Oxygen (O2) Calibrated Probe
    where:   

           Libelium Smart Cities Top
           --------------------
             .A   .B   .C
             .D   .E   .F
           --------------------
           Device Specs Bottom

   This code is based off a combination of example codes:
    - WIFI_PRO_01_configure_essid
    - WIFI_PRO_22_time
    - WIFI_PRO_26_ssl_sockets
    - SCP_v30_01_Electrochemical_gas_sensors

   There were also a few changes to firmware code made:
   The PDE file WaspWIFI_PRO was also edited 
   to disable SSL CA certificate requirements.
   NOTE: line 6189 -> printString("AT+iSDM=128\r", _uart); 

   Enabled debug option in gas pro header file:
   #define GAS_DEBUG 2 (instead of 0) in WaspSensorGas_Pro.h
   
   ** IGNORE WARNING MESSAGES on compile

  Bill Zhou & Gilbert Oppy
*/

// Put your libraries here (#include ...)
#include <WaspWIFI_PRO.h>
#include <WaspFrame.h>
#include <WaspSensorCities_PRO.h>
#include <WaspSensorGas_Pro.h>

/*
   Define object for sensor: gas_PRO_sensor
   Input to choose board socket.
   Waspmote OEM. Possibilities for this sensor:
    - SOCKET_1
    - SOCKET_3
    - SOCKET_5
   P&S! Possibilities for this sensor:
    - SOCKET_B
    - SOCKET_C
    - SOCKET_F
*/
Gas gas_PRO_sensor(SOCKET_C);

// choose socket (SELECT USER'S SOCKET)
///////////////////////////////////////
uint8_t socket = SOCKET0;
///////////////////////////////////////

// WiFi AP settings (CHANGE TO USER'S AP)
///////////////////////////////////////
char ESSID[] = "RangeTest";
char PASSW[] = "ThePrincess";
/////////////////////////////////

// choose TCP server settings
///////////////////////////////////////
char HOST[]        = "LifeOfGurrowa.azure-devices.net";
char REMOTE_PORT[] = "443";
char LOCAL_PORT[]  = "3000";
char *token = "Authorization: SharedAccessSignature sr=LifeOfGurrowa.azure-devices.net&sig=utFydlKTrUaDLxZ5qjdDHabodqiaGbBn11GiHCEofMc%3D&se=1530833052&skn=device";



// Message Construction for POSTing to Azure
//char *msgPattern = "{\"StationID\":%s,\"Timestamp\":\"%s\",\"BatteryLevel\":\"%s\",\"Temperature\":%s,\"Pressure\":%s,\"Humidity\":%s,\"O2\":%s}";
char *msgPattern = "{ 'StationID' : '%s' , 'Timestamp' : '%s' , 'BatteryLevel' : '%s' , 'Temperature' : '%s' , 'Pressure' : '%s', 'Humidity' : '%s', 'O2' : '%s' }";
///////////////////////////////////////

// define data to send through TCP socket
///////////////////////////////////////
char requestBuffer[550]; // Needs to be larger than about 500, see length of string

// define certificate for SSL (ULTIMATELY NOT USED)
////////////////////////////////////////////////////////////////////////

char TRUSTED_CA[] =//
  "-----BEGIN CERTIFICATE-----\r"\
  "MIICNjCCAZ+gAwIBAgIJAL5/5O7w2Cm5MA0GCSqGSIb3DQEBCwUAMFMxLTArBgNV\r"\
  "BAoMJExpYmVsaXVtIENvbXVuaWNhY2lvbmVzIERpc3RyaWJ1aWRhczELMAkGA1UE\r"\
  "BhMCRVMxFTATBgNVBAMMDGxpYmVsaXVtLmNvbTAgFw0xNzAxMjQwOTU1MDJaGA8y\r"\
  "MTE2MTIzMTA5NTUwMlowUzEtMCsGA1UECgwkTGliZWxpdW0gQ29tdW5pY2FjaW9u\r"\
  "ZXMgRGlzdHJpYnVpZGFzMQswCQYDVQQGEwJFUzEVMBMGA1UEAwwMbGliZWxpdW0u\r"\
  "Y29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCfI9j2DbbuK1fUrH1RKmnU\r"\
  "EQ22r7FAT+R7uxOhSBnx61qlLjtZT9zuA7eMuq9k3tUBSkMxJjai6ebqmvPUpgrU\r"\
  "0EoCZg+PrAglcvqAkzv8QDRueEi0hLCa8iTUsuora3viKMefbFR8ROH5uQrFnZK1\r"\
  "1aUQxeV0HBL9zIH8ghaLmwIDAQABoxAwDjAMBgNVHRMEBTADAQH/MA0GCSqGSIb3\r"\
  "DQEBCwUAA4GBAG2bWPWsfHzSqwlzY+5kJDeAgJ7GfQo51+QXqMq5nnjcPhgkIkvz\r"\
  "IVOO2WM01Pnm3LuEQ3YS8eHS1blOL8i7GsxxIMR6aQ8E0XYbcizPvcyL+NAdIodd\r"\
  "bSa087amkPIVcwETuGf2JdpbJLEjWayfcE1Ll+DA7UfX9korLzZzfDxX\r"\
  "-----END CERTIFICATE-----";
//"\r";

////////////////////////////////////////////////////////////////////////

// choose NTP server settings
///////////////////////////////////////
char SERVER1[] = "time.nist.gov";
char SERVER2[] = "wwv.nist.gov";
///////////////////////////////////////

// USER VARIABLES
uint8_t error;
uint8_t status;
unsigned long previous;
uint16_t socket_handle = 0;
char deviceId[20];
float concentration;  // Stores the concentration level in ppm
float temperature;  // Stores the temperature in ºC
float humidity;   // Stores the realitve humidity in %RH
float pressure;   // Stores the pressure in Pa
float BatteryLevel; //Stores the battery Level


char concentrationStr[10];  // Stores the concentration level in ppm
char temperatureStr[10];  // Stores the temperature in ºC
char humidityStr[10];   // Stores the realitve humidity in %RH
char pressureStr[10];   // Stores the pressure in Pa
char BatteryLevelStr[10];  // Stores the Battery Level in %

void makeRequest();

void setup()
{
  Utils.setLED(LED1, LED_ON); 
  USB.println(F("Start program"));
  USB.println(F("***************************************"));
  USB.println(F("Once the module is set with one or more"));
  USB.println(F("AP settings, it attempts to join the AP"));
  USB.println(F("automatically once it is powered on"));
  USB.println(F("Refer to example 'WIFI_PRO_01' to configure"));
  USB.println(F("the WiFi module with proper settings"));
  USB.println(F("***************************************"));

  // Reading the serial number
  Utils.readSerialID();

  // Set Device name for IoTHub as the Serial Id of the device
  USB.print(F("deviceName on IoT Hub: "));
  sprintf(deviceId, "%02X%02X%02X%02X%02X%02X%02X%02X", _serial_id[0], _serial_id[1], _serial_id[2], _serial_id[3], _serial_id[4], _serial_id[5], _serial_id[6], _serial_id[7]);
  USB.println(deviceId);


  //////////////////////////////////////////////////
  // A. Switch ON WIFI
  //////////////////////////////////////////////////
  error = WIFI_PRO.ON(socket);

  if ( error == 0 )
  {
    USB.println(F("A. WiFi switched ON"));
  }
  else
  {
    USB.println(F("A. WiFi did not initialize correctly"));
    Utils.blinkRedLED(200, 20);
    Utils.setLED(LED1, LED_ON); 
  }

  //////////////////////////////////////////////////
  // A.1 Reset to default values
  //////////////////////////////////////////////////
  error = WIFI_PRO.resetValues();

  if (error == 0)
  {
    USB.println(F("A.1 WiFi reset to default"));
  }
  else
  {
    USB.println(F("A.1 WiFi reset to default ERROR"));
  }


  //////////////////////////////////////////////////
  // B. Set ESSID
  //////////////////////////////////////////////////
  error = WIFI_PRO.setESSID(ESSID);

  if (error == 0)
  {
    USB.println(F("B. WiFi set ESSID OK"));
  }
  else
  {
    USB.println(F("B. WiFi set ESSID ERROR"));
  }


  //////////////////////////////////////////////////
  // B.1 Set password key (It takes a while to generate the key)
  // Authentication modes:
  //    OPEN: no security
  //    WEP64: WEP 64
  //    WEP128: WEP 128
  //    WPA: WPA-PSK with TKIP encryption
  //    WPA2: WPA2-PSK with TKIP or AES encryption
  //////////////////////////////////////////////////
  error = WIFI_PRO.setPassword(WPA2, PASSW);

  if (error == 0)
  {
    USB.println(F("B.1 WiFi set AUTHKEY OK"));
  }
  else
  {
    USB.println(F("B.1 WiFi set AUTHKEY ERROR"));
  }

  USB.println(F("*******************************************"));
  USB.println(F("Once the module is configured with ESSID"));
  USB.println(F("and PASSWORD, the module will attempt to "));
  USB.println(F("join the specified Access Point on power up"));
  USB.println(F("*******************************************"));

  //////////////////////////////////////////////////
  // C. Set Trusted CA (WIFI_PRO configured to disable SSL)
  //////////////////////////////////////////////////
  error = WIFI_PRO.setCA(TRUSTED_CA);

  if (error == 0)
  {
    USB.println(F("C. Trusted CA set OK"));
  }
  else
  {
    USB.println(F("C. Error calling 'setCA' function"));
    WIFI_PRO.printErrorCode();
  }

  //////////////////////////////////////////////////
  // D. Switch OFF WIFI
  //////////////////////////////////////////////////
  USB.println(F("D. WiFi switched OFF"));
  WIFI_PRO.OFF(socket);
  USB.println(F("E. Turning on sockets, makes sure temperature, humidity and pressure sensor is in socket B"));
  USB.println(F("*******************************************"));
  ///////////////////////////////////////////
  // E. Turn on the sensors and RTC
  ///////////////////////////////////////////

  // Power on the socket 1 for the gas sensor
  SensorCitiesPRO.ON(SOCKET_C);
  // Power on the socket 2 for the temperature sensor
  SensorCitiesPRO.ON(SOCKET_B);

  // Power on the electrochemical sensor. It also boots the temperature sensor
  gas_PRO_sensor.ON();

  // Turn on Real time Clock
  RTC.ON();

}



void loop()
{
  //////////////////////////////////////////////////
  // 1. Switch ON WIFI
  //////////////////////////////////////////////////
  error = WIFI_PRO.ON(socket);

  if ( error == 0 )
  {
    USB.println(F("1. WiFi switched ON"));
  }
  else
  {
    USB.println(F("1. WiFi did not initialize correctly"));
    Utils.blinkRedLED(200, 20);
    Utils.setLED(LED1, LED_ON); 
  }


  //////////////////////////////////////////////////
  // 2. Check if connected
  //////////////////////////////////////////////////

  // get actual time
  previous = millis();

  // check connectivity
  status =  WIFI_PRO.isConnected();

  // check if module is connected
  if ( status == true )
  {
    USB.print(F("2. WiFi is connected OK"));
    USB.print(F(" Time(ms):"));
    USB.println(millis() - previous);

    
    // get IP address
    error = WIFI_PRO.getIP();

    if (error == 0)
    {
      USB.print(F("2. IP address: "));
      USB.println( WIFI_PRO._ip );
    }
    else
    {
      USB.println(F("getIP error"));
    }
  }
  else
  {
    USB.print(F("2. WiFi is connected ERROR"));
    USB.print(F(" Time(ms):"));
    USB.println(millis() - previous);
  }

  //////////////////////////////////////////////////
  // 3. NTP server
  //////////////////////////////////////////////////

  // 3.1. Set NTP Server (option1)
  error = WIFI_PRO.setTimeServer(1, SERVER1);

  // check response
  if (error == 0)
  {
    USB.println(F("3.1. Time Server1 set OK"));
  }
  else
  {
    USB.println(F("3.1. Error calling 'setTimeServer' function"));
    WIFI_PRO.printErrorCode();
    status = false;
  }


  // 3.2. Set NTP Server (option2)
  error = WIFI_PRO.setTimeServer(2, SERVER2);

  // check response
  if (error == 0)
  {
    USB.println(F("3.2. Time Server2 set OK"));
  }
  else
  {
    USB.println(F("3.2. Error calling 'setTimeServer' function"));
    WIFI_PRO.printErrorCode();
    status = false;
  }

  // 3.3. Enabled/Disable Time Sync
  if (status == true)
  {
    error = WIFI_PRO.timeActivationFlag(true);

    // check response
    if ( error == 0 )
    {
      USB.println(F("3.3. Network Time-of-Day Activation Flag set OK"));
    }
    else
    {
      USB.println(F("3.3. Error calling 'timeActivationFlag' function"));
      WIFI_PRO.printErrorCode();
      status = false;
    }
  }



  //////////////////////////////////////////////////
  // 4. Set RTC Time from WiFi module settings
  //////////////////////////////////////////////////

  // Check if module is connected
  if (status == true)
  {
    // 3.1. Open FTP session
    int error = WIFI_PRO.setTimeFromWIFI();

    // check response
    if (error == 0)
    {
      USB.print(F("4. Set RTC time OK. Time:"));
      USB.println(RTC.getTime());
    }
    else
    {
      USB.println(F("4. Error calling 'setTimeFromWIFI' function"));
      WIFI_PRO.printErrorCode();

      USB.print(F("Current RTC settings:"));
      USB.println(RTC.getTime());
    }
  }

  //////////////////////////////////////////////////
  // 5. TCP
  //////////////////////////////////////////////////

  // Check if module is connected
  if (status == true)
  {
    ////////////////////////////////////////////////
    // 5.1. Open TCP socket
    ////////////////////////////////////////////////
    error = WIFI_PRO.setTCPclient( HOST, REMOTE_PORT, LOCAL_PORT);

    // check response
    if (error == 0)
    {
      // get socket handle (from 0 to 9)
      socket_handle = WIFI_PRO._socket_handle;

      USB.print(F("5.1. Open TCP socket OK in handle: "));
      USB.println(socket_handle, DEC);
    }
    else
    {
      USB.println(F("5.1. Error calling 'setTCPclient' function"));
      WIFI_PRO.printErrorCode();
      status = false;
    }

    if (status == true)
    {
      ////////////////////////////////////////////////
      // 5.2. SSL hand shake
      ////////////////////////////////////////////////
      error = WIFI_PRO.sslHandshake(WIFI_PRO._socket_handle);

      if (error == 0)
      {
        USB.println(F("5.2. SSL handle shake OK"));
      }
      else
      {
        USB.println(F("5.2. Error calling 'sslHandshake' function"));
        WIFI_PRO.printErrorCode();
      }

      ////////////////////////////////////////////////
      // 5.3. send data
      ////////////////////////////////////////////////
      makeRequest();
      error = WIFI_PRO.send( socket_handle, requestBuffer);

      // check response
      if (error == 0)
      {
        USB.println(F("5.3. Send data OK"));
      }
      else
      {
        USB.println(F("5.3. Error calling 'send' function"));
        WIFI_PRO.printErrorCode();
      }

      ////////////////////////////////////////////////
      // 5.4. Wait for answer from server
      ////////////////////////////////////////////////
      USB.println(F("Listen to TCP socket:"));
      error = WIFI_PRO.receive(socket_handle, 30000);

      // check answer
      if (error == 0)
      {
        USB.println(F("\n========================================"));
        USB.print(F("Data: "));
        USB.println( WIFI_PRO._buffer, WIFI_PRO._length);

        USB.print(F("Length: "));
        USB.println( WIFI_PRO._length, DEC);
        USB.println(F("========================================"));
      }

      ////////////////////////////////////////////////
      // 5.5. close socket
      ////////////////////////////////////////////////
      error = WIFI_PRO.closeSocket(socket_handle);

      // check response
      if (error == 0)
      {
        USB.println(F("5.5. Close socket OK"));
      }
      else
      {
        USB.println(F("5.5. Error calling 'closeSocket' function"));
        WIFI_PRO.printErrorCode();
      }
    }
  }


  //////////////////////////////////////////////////
  // 6. Switch OFF WIFI
  //////////////////////////////////////////////////
  USB.println(F("6. WiFi switched OFF\n\n"));
  WIFI_PRO.OFF(socket);

  USB.println(F("Wait 45 seconds...\n"));
  delay(45000);

}

  //////////////////////////////////////////////////
  // FUNCTION FOR GRABBING SENSOR DATA AND POSTING TO AZURE
  //////////////////////////////////////////////////

void makeRequest() {
  int i;
  char *TARGET_URL = "/devices/";
  char *IOT_HUB_END_POINT = "/messages/events?api-version=2016-11-14";
  char msg[200];



  /// Construct strings for HTTP packet
  //POST " + endPoint + " HTTP/1.1\r\n
  i = sprintf(requestBuffer, "POST %s%s%s HTTP/1.1\r\n", TARGET_URL, deviceId, IOT_HUB_END_POINT);
  //"Host: " + host + "\r\n"
  i += sprintf(requestBuffer + i, "host: %s\r\n", HOST);


  strcpy(requestBuffer + i, token);
  i += strlen(token);

  i += sprintf(requestBuffer + i, "\r\n");

  i += sprintf(requestBuffer + i, "%s\r\n", "Content-Type: application/atom+xml;type=entry;charset=utf-8");

  ///////////////////////////////////////////
  // Read sensors
  ///////////////////////////////////////////

  // Read the electrochemical sensor and compensate with the temperature internally
  concentration = gas_PRO_sensor.getConc()/10000;

  // Read enviromental variables
  temperature = SensorCitiesPRO.getTemperature();
  humidity = SensorCitiesPRO.getHumidity();
  pressure = SensorCitiesPRO.getPressure()/1000;
  BatteryLevel = PWR.getBatteryLevel(); 

  // And print the values via USB
  USB.println(F("***************************************"));
  USB.print(F("Gas concentration: "));
  USB.print(concentration);
  USB.println(F(" ppm"));

  USB.print(F("Temperature: "));
  USB.print(temperature);
  USB.println(F(" Celsius degrees"));
  USB.print(F("RH: "));
  USB.print(humidity);
  USB.println(F(" %"));
  USB.print(F("Pressure: "));
  USB.print(pressure);
  USB.println(F(" Pa"));
  
  // Show the remaining battery level
  USB.print(F("Battery Level: "));
  USB.print(BatteryLevel,DEC);
  USB.print(F(" %"));
  
  Utils.float2String(concentration, concentrationStr, 3);
  Utils.float2String(temperature, temperatureStr, 3);
  Utils.float2String(humidity, humidityStr, 3);
  Utils.float2String(pressure, pressureStr, 3);
  Utils.float2String(BatteryLevel, BatteryLevelStr, 3);

  //char *msgPattern = "{\“StationID\”:%s,\“Timestamp\”:%s,\“Temperature\”:%s,\“Pressure\”:%s,\“Humidity\”:%s,\“O2\”:%s}";
  RTC.getTime();

  sprintf(msg, msgPattern, deviceId, RTC.timeStamp, BatteryLevelStr, temperatureStr, pressureStr, humidityStr, concentrationStr);

  i += sprintf(requestBuffer + i, "Content-Length: %d\r\n\r\n", strlen(msg));

  i += sprintf(requestBuffer + i, "%s", msg);

  USB.print(F("Total length:"));
  USB.println(i);
  USB.println(requestBuffer);
}
